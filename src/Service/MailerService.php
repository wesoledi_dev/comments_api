<?php

namespace App\Service;

use App\Service\Util\EnviromentCheck;

class MailerService
{
    private const DEFAULT_PORT = 465;
    private const DEFAULT_ENCRYPTION = 'ssl';
    private const DEFAULT_AUTH_MODE = 'login';

    /** return bool true if successful */
    public static function sendMessage(
        string $recipient,
        string $subject,
        string $message
    ): bool {
        $transport = \Swift_SmtpTransport::newInstance(
            self::getMailHost(), 
            self::getPort(), 
            self::getEncryption()
            )
            ->setUsername(self::getUsername())
            ->setPassword(self::getPassword())
            ->setAuthMode(self::getAuthMode());

        if (EnviromentCheck::isDev()) {
            $transport->setLocalDomain('[127.0.0.1]');
        }

        $mailer = \Swift_Mailer::newInstance($transport);

        $messages = \Swift_Message::newInstance($subject)
            ->setFrom(self::getUsername())
            ->setTo($recipient)
            ->setContentType("text/html; charset=UTF-8")
            ->setBody($message, 'text/html');

        return 0 !== $mailer->send($messages);
    }

    private static function getMailHost(): string
    {
        return $_ENV['MAILER_HOST'] ?? '';
    }

    private static function getUsername(): string
    {
        return $_ENV['MAILER_USERNAME'] ?? '';
    }

    private static function getPassword(): string
    {
        return $_ENV['MAILER_PASSWORD'] ?? '';
    }

    private static function getAuthMode(): string
    {
        return $_ENV['MAILER_AUTH_MODE'] ?? self::DEFAULT_AUTH_MODE;
    }

    private static function getPort(): string
    {
        return $_ENV['MAILER_PORT'] ?? self::DEFAULT_PORT;
    }

    private static function getEncryption(): string
    {
        return $_ENV['MAILER_ENCRYPTION'] ?? self::DEFAULT_ENCRYPTION;
    }
}