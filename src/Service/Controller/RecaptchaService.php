<?php

namespace App\Service\Controller;

use App\Service\ErrorCollector;
use App\Service\HttpClient\RecaptchaClient;

class RecaptchaService
{
    public static function getSecret(string $site): string
    {
        $envKey = 'RECAPTCHA_SECRET_' . strtoupper($site);
        return $_ENV[$envKey] ?? '';
    }

    public static function verify(string $token, string $secret): bool
    {
        try {
            $response = (new RecaptchaClient())->post([
                'response' => $token,
                'secret' => $secret
            ]);

            $result = $response->getContent()['success'];

            return $result;
        } catch (\Exception $e) {
            ErrorCollector::saveWithException('RECAPTCHA VERIFICATION FAIL', $e, [
                'token/response' => $token,
                'secret' => $secret,
            ]);
            return false;
        }
    }
}