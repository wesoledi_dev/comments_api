<?php

namespace App\Service\Controller;

use App\Dictionary\HttpStatus;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseBuilder
{
    public static function simpleClear(int $status = HttpStatus::HTTP_OK): JsonResponse
    {
        $message = self::getBaseMessage($status);
        return new JsonResponse($message, $status);
    }

    public static function simpleArray(array $dataArray, int $status = HttpStatus::HTTP_OK): JsonResponse
    {
        $message = self::getBaseMessage($status);
        $message = array_merge($message, ['data' => $dataArray]);
        return new JsonResponse($message, $status);
    }

    private static function getBaseMessage(int $status): array
    {
        return [
            'status_code' => $status,
            'status_text' => HttpStatus::getStatusTextValue($status),
        ];
    }

    public static function convertEntity(object $entity): array
    {
        return method_exists($entity, 'toArray') ?
            $entity->toArray() :
            ['inner-error' => 'conversion unavailable'];
    }

    public static function convertEntitiesList(array $entities): array
    {
        $result = [];

        foreach ($entities as $entity) {
            $result[] = self::convertEntity($entity);
        }

        return $result;
    }
}