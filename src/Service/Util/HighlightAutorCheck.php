<?php

namespace App\Service\Util;

use Symfony\Component\HttpFoundation\Cookie;

class HighlightAutorCheck
{
    public static function isArticleAuthor(): bool
    {
        $cookieName = $_ENV['COOKIE_AUTHOR'];
        $cookie = $_COOKIE[$cookieName] ?? 'comment_author_special';
        return self::verify($cookie);
    }

    private static function verify(string $cookieContent): bool
    {
        $savedVerificationCode = $_ENV['COOKIE_AUTHOR_VERIFICATION_CODE'];
        if (empty($savedVerificationCode)) {
            $savedVerificationCode = (string)rand(1000, 9999);
        }

        return $cookieContent === $savedVerificationCode;
    }
}