<?php

namespace App\Service\Util;

class EnviromentCheck
{
    public static function isDev(): bool
    {
        return strtolower($_ENV['APP_ENV'] ?? '') === 'dev';
    }
}