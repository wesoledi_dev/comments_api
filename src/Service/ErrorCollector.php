<?php

namespace App\Service;

class ErrorCollector
{
    public static function save(string $message, array $content): bool
    {
        self::saveWithException($message, null, $content);
        
        return true;
    }

    public static function saveWithException(string $message, ?\Exception $e, array $content): bool
    {
        switch ($_ENV['LOGGER']) {
            case 'FILE':
            default: 
                $text = sprintf("%s: %s", $message, json_encode($content));
                $text .= $e ? self::getExceptionAsString($e) : '';
                
                self::saveToFile($text);
                break;
        }
        
        return true;
    }

    private static function getExceptionAsString(\Exception $e): string 
    {
        return '/ EXCEPTION DATA: ' . sprintf(
            'message: %s, file: %s, at line: %d',
            $e->getMessage(), 
            $e->getFile(),
            $e->getLine()
        );
    }

    private static function saveToFile(string $content): void
    {
        $logFile = fopen("error_log.txt", "a");
        if (!$logFile) {
            return;
        }
        fwrite($logFile, sprintf("%s - %s\r\n", date('Y-m-d H:i:s'), $content));
        fclose($logFile);
    }
}