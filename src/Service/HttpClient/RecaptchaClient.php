<?php

namespace App\Service\HttpClient;

use App\Entity\App\HttpClientResponse;
use Symfony\Component\HttpFoundation\Request;

class RecaptchaClient extends Client
{
    public function __construct()
    {
        parent::__construct('https://www.google.com');
    }

    public function post(array $payload): HttpClientResponse
    {
        return $this->request(Request::METHOD_POST, '/recaptcha/api/siteverify', $payload);
    }
}