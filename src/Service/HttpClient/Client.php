<?php

namespace App\Service\HttpClient;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\App\HttpClientResponse;

class Client 
{
    private HttpClientInterface $client;

    public function __construct(string $baseUri)
    {
        $this->client = HttpClient::createForBaseUri($baseUri);
    }

    protected function request(string $method = Request::METHOD_GET, string $uri = '/', array $payload = [])
    {
        $response = $this->client->request(
            $method, 
            $this->getPreparedUri($uri),
            (!empty($payload) && $method !== Request::METHOD_GET) ? ['body' => $payload] : []
        );
        $statusCode = $response->getStatusCode();
        $content = $response->toArray();

        return new HttpClientResponse($statusCode, $content);
    }

    private function getPreparedUri(string $uriPart): string
    {
        if (substr($uriPart, 0, 1) !== '/') {
            $uriPart = '/' . $uriPart;
        }
        return $uriPart;
    }
}