<?php

namespace App\Dictionary\Comments;

class BannedRemoteAddrs {

    public static function isBanned(string $addr): bool
    {
        $list = explode('|', ($_ENV['BANNED_ADDRS'] ?? ''));

        return empty($list) ? false : in_array($addr, $list);
    }
}