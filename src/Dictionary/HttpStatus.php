<?php

namespace App\Dictionary;

class HttpStatus
{
    // 200 - OK
    public const HTTP_OK = 200;
    public const HTTP_OK_NO_CONTENT = 204;

    // 400 - Client Error
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_UNAUTHORIZED = 401;
    public const HTTP_FORBIDDEN = 403;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_NOT_ACCEPTABLE = 406;
    public const HTTP_CONFLICT = 409;
    public const HTTP_UNSUPPORTED_MEDIA = 415;
    public const HTTP_UNPROCESSABLE = 422;

    // 500 - Server Error
    public const HTTP_ERROR_INTERNAL = 500;
    public const HTTP_ERROR_NOT_IMPLEMENTED = 501;
    public const HTTP_ERROR_BAD_GATEWAY = 502;
    public const HTTP_ERROR_SERVICE_UNAVAILABLE = 503;

    // text value
    private const HTTP_STATUS_TEXT = [
        self::HTTP_OK => 'OK',
        self::HTTP_OK_NO_CONTENT => 'OK - no data',
        self::HTTP_BAD_REQUEST => 'Bad request params or payload',
        self::HTTP_UNAUTHORIZED => 'Authorization required',
        self::HTTP_FORBIDDEN => 'Not allowed',
        self::HTTP_NOT_FOUND => 'Not found',
        self::HTTP_NOT_ACCEPTABLE => 'Not acceptable',
        self::HTTP_CONFLICT => 'Conflict found',
        self::HTTP_UNSUPPORTED_MEDIA => 'Wrong media type',
        self::HTTP_UNPROCESSABLE => "Can't be processed",
        self::HTTP_ERROR_INTERNAL => 'Error',
        self::HTTP_ERROR_NOT_IMPLEMENTED => 'Action not ready yet',
        self::HTTP_ERROR_BAD_GATEWAY => 'Communication error',
        self::HTTP_ERROR_SERVICE_UNAVAILABLE => 'Service unavailable',
    ];

    public static function getStatusTextValue(int $statusCode): string
    {
        return self::HTTP_STATUS_TEXT[$statusCode] ?? 'Unknown';
    }
}