<?php

namespace App\Repository;

use App\Dictionary\Comments\BannedRemoteAddrs;
use App\Entity\Comments;
use App\Service\ErrorCollector;
use App\Service\Util\HighlightAutorCheck;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comments|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comments|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comments[]    findAll()
 * @method Comments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comments::class);
    }

    public static function build(
        string $author, 
        string $content,
        string $site = 'none',
        string $article = 'none'
    ): ?Comments {
        $remoteAddr = $_SERVER['REMOTE_ADDR'] ?? 'n/a';
        if (BannedRemoteAddrs::isBanned($remoteAddr)) {
            ErrorCollector::save('AUTHOR BANNED', ['remote_addr' => $remoteAddr]);
            return null;
        }

        $comment = new Comments();
        $comment->setSiteName($site);
        $comment->setArticleName($article);
        $comment->setAuthor($author);
        $comment->setText($content);
        $comment->setTime((new DateTime()));
        $comment->setAuthorId($remoteAddr);
        $comment->setHighlight(HighlightAutorCheck::isArticleAuthor());

        return $comment;
    }
}
