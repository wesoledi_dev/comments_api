<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

class RequestValidator 
{
    private const API_KEY_NAME = 's-api-key';
    private const RESTRICTED_METHODS = ['POST', 'PUT', 'DELETE'];

    public static function canProcced(Request $request): bool
    {
        if (in_array($request->getMethod(), self::RESTRICTED_METHODS)) {
            $requestApiKey = $request->headers->get(self::API_KEY_NAME);
            return $requestApiKey === $_ENV['API_KEY'];
        }

        return true;
    }
}