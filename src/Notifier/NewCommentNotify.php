<?php

namespace App\Notifier;

use App\Service\MailerService;

class NewCommentNotify
{
    private const SUBJECT = '';

    public function notify(string $site, string $author, string $content): void
    {
        MailerService::sendMessage(
            $this->getRecipient(), 
            $this->getSubject($site), 
            $this->getNotifyMessage($author, $content)
        );
    }

    private function getRecipient(): string
    {
        return $_ENV['NOTIFY_RECIPIENT'] ?? '';
    }

    private function getSubject($site): string
    {
        return sprintf(
            'New comment on: %s',
            $site
        );
    }

    private function getNotifyMessage(string $author, string $content): string
    {
        return sprintf(
            'Someone named: %s<br/>Send comment: %s',
            $author, 
            $content
        );
    }
}