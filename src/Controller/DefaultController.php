<?php

namespace App\Controller;

use App\Service\Controller\ResponseBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function index()
    {
        return ResponseBuilder::simpleArray([
            'message' => 'Hello!',
            'version' => ($_ENV['CURRENT_VERSION'] ?? 'n/d')
        ]);
    }
}