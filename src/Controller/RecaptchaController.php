<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Controller\ResponseBuilder;
use App\Service\Controller\RecaptchaService;

class RecaptchaController extends AbstractController
{
    public function verify(Request $request)
    {
        $postData = json_decode($request->getContent(), true);
        $site = $postData['site'] ?? '';
        $token = $postData['token'] ?? '';

        $result = RecaptchaService::verify($token, RecaptchaService::getSecret($site));
        
        return ResponseBuilder::simpleArray(['result' => $result]);
    }
}