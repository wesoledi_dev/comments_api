<?php

namespace App\Controller;

use App\Dictionary\HttpStatus;
use App\Notifier\NewCommentNotify;
use App\Service\Controller\ResponseBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CommentsRepository;
use App\Service\ErrorCollector;
use Symfony\Component\HttpFoundation\Request;

class CommentsController extends AbstractController
{
    public function list(string $site, string $article)
    {
        $criteria = [
            'siteName' => $site,
            'articleName' => $article,
        ];

        try {
            $comments = (new CommentsRepository($this->getDoctrine()))->findBy($criteria);
        } catch (\Exception $e) {
            ErrorCollector::save($e->getMessage(), []);
            return ResponseBuilder::simpleClear(HttpStatus::HTTP_ERROR_BAD_GATEWAY);
        }

        return ResponseBuilder::simpleArray([
            'params' => [
                'site' => $site,
                'article' => $article,
            ],
            'comments' => ResponseBuilder::convertEntitiesList($comments),
        ]);
    }

    public function new(Request $request)
    {
        $postData = json_decode($request->getContent(), true);
        $author = $postData['author'] ?? '';
        $content = $postData['content'] ?? '';
        $site = $postData['site'] ?? '';
        $article = $postData['article'] ?? '';

        try {
            $comment = CommentsRepository::build(
                $author,
                $content,
                $site,
                $article,
            );
        } catch (\Exception $e) {
            ErrorCollector::saveWithException($e->getMessage(), $e, $postData);
            return ResponseBuilder::simpleClear(HttpStatus::HTTP_ERROR_INTERNAL);
        }

        if (!$comment) {
            return ResponseBuilder::simpleClear(HttpStatus::HTTP_FORBIDDEN);
        }

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
        } catch (\Exception $e) {
            ErrorCollector::save($e->getMessage(), []);
            return ResponseBuilder::simpleClear(HttpStatus::HTTP_ERROR_BAD_GATEWAY);
        }

        (new NewCommentNotify())->notify($site . '/' . $article, $author, $content);

        return ResponseBuilder::simpleClear();
    }
}