<?php

require_once "vendor/symfony/dotenv/Dotenv.php";

$dotenv = new Symfony\Component\Dotenv\Dotenv();
$dotenv->load(__DIR__.'/.env', __DIR__.'/.env.local');

define("API_HOST", ($_ENV['APP_ENV'] ?? '' === "dev") ? "localhost" : "stareaparaty.net");
define("API_VERSION", $_ENV['CURRENT_VERSION'] ?? '');
