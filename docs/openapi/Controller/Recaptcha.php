<?php

/**
*   @OA\Post(
*       path="/recaptcha",
*       @OA\Response(
*           response="200",
*           description="Verify ReCaptcha for specific site",
*           @OA\JsonContent(
*               @OA\Property(
*                   property="data",
*                   example="{result:true}"
*               ),
*               schema="#components/schemas/ResponseBuilder_array",
*           ),
*       ),
*       @OA\RequestBody(
*           description="Recaptcha params",
*           required=true,
*           @OA\JsonContent(
*               @OA\Property(
*                   property="site",
*                   type="string",
*                   description="site_identifier",
*               ),
*               @OA\Property(
*                   property="token",
*                   type="string",
*                   description="app token from front-end application",
*               ),
*           ),
*       ),
*   )
*/