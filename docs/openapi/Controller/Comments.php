<?php

/**
*   @OA\Get(
*       path="/list/{site}/{article}",
*       @OA\Response(
*           response="200",
*           description="List of articles for provided site and article",
*           @OA\JsonContent(
*               @OA\Property(
*                   property="data",
*                   example="{params:'{site, article}', comments:[{ #Comments }]}",
*                   schema="#components/schemas/Comments",
*               ),
*               schema="#components/schemas/ResponseBuilder_array",
*           ),
*       ),
*   )
*/

/**
*   @OA\Post(
*       path="/new",
*       @OA\Response(
*           response="200",
*           description="Add new comments",
*           @OA\JsonContent(
*               schema="#components/schemas/ResponseBuilder_simple",
*           ),
*       ),
*       @OA\Response(
*           response="500",
*           description="Internal error - propably a wrong request json structure",
*           @OA\JsonContent(
*               schema="#components/schemas/ResponseBuilder_simple",
*           ),
*       ),
*       @OA\Response(
*           response="502",
*           description="Internal error - propably cannot connect to database",
*           @OA\JsonContent(
*               schema="#components/schemas/ResponseBuilder_simple",
*           ),
*       ),
*       @OA\Response(
*           response="403",
*           description="Forbidden - cannot add a comment",
*           @OA\JsonContent(
*               schema="#components/schemas/ResponseBuilder_simple",
*           ),
*       ),
*       @OA\RequestBody(
*           description="New comment body",
*           required=true,
*           @OA\JsonContent(
*               @OA\Property(
*                   property="site",
*                   type="string",
*                   description="site_identifier",
*               ),
*               @OA\Property(
*                   property="article",
*                   type="string",
*                   description="article_identifier",
*               ),
*               @OA\Property(
*                   property="author",
*                   type="string",
*                   description="author name (max 50)",
*               ),
*               @OA\Property(
*                   property="content",
*                   type="string",
*                   description="content text (max 1024)",
*               ),
*           ),
*       ),
*   )
*/