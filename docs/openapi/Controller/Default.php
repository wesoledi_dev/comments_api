<?php

/**
*   @OA\Get(
*       path="/",
*       @OA\Response(
*           response="200",
*           description="Hello message with Current Version",
*           @OA\JsonContent(
*               @OA\Property(
*                   property="data",
*                   example="{message:'Hello!', version:'1.1'}"
*               ),
*               schema="#components/schemas/ResponseBuilder_array",
*           ),
*       )
*   )
*/