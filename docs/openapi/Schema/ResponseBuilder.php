<?php

/**
* @OA\Schema(
*   schema="ResponseBuilder_simple",
*   type="array",
*   format="json",
*   description="Simple response, with status code",
*   items={
*       "status_code"=@OA\Items(type="int", description="HTTP Status Code"),
*       "status_name"=@OA\Items(type="string", description="HTTP Status Code descripted"),
*   }
* )
*/

/**
* @OA\Schema(
*   schema="ResponseBuilder_array",
*   type="array",
*   format="json",
*   description="Simple response, with status code and 'data' property as array",
*   items={
*       "status_code"=@OA\Items(type="int", description="HTTP Status Code"),
*       "status_name"=@OA\Items(type="string", description="HTTP Status Code descripted"),
*       "data"=@OA\Items(type="array", @OA\Items(type={}, description="Mixed data type")),
*   }
* )
*/