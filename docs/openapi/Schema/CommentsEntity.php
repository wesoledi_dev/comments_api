<?php

/**
*   @OA\Schema(
*       schema="Comments",
*       @OA\Property(
*           property="id",
*           type="integer",
*           description="Comment id"
*       ),
*       @OA\Property(
*           property="text",
*           type="string",
*           description="Comment content"
*       ),
*       @OA\Property(
*           property="time",
*           type="datetime",
*           description="Comment publish date with time"
*       ),
*       @OA\Property(
*           property="author",
*           type="string",
*           description="Name of author"
*       ),
*       @OA\Property(
*           property="highlight",
*           type="integer",
*           description="Does and how, comment should be highlighted"
*       ),
*  )
*/